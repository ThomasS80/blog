<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200323174905 extends AbstractMigration
{
    public function Description() : string
    {
        return '';
    }

    public function MiseAJour(Schema $insertion) : void
    {
       
        $this->abortIf($this->connexion->getDatabasePlatform()->getName() !== 'mysql', 'La migration peut etre faite en \'mysql\'.');

        $this->addSql('CREATE TABLE Post (id INT AUTO_INCREMENT NOT NULL, author_id INT NOT NULL, content LONGTEXT NOT NULL, title VARCHAR(255) NOT NULL, Publication DATETIME NOT NULL, edition_date DATETIME NOT NULL, INDEX IDX_B655067AF675F31B (author_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Utilisateur (id INT AUTO_INCREMENT NOT NULL, Prenom VARCHAR(180) NOT NULL, Role JSON NOT NULL, password VARCHAR(255) NOT NULL, mail VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_614CBCBEF85E0677 (Prenom), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Retourne (id INT AUTO_INCREMENT NOT NULL, post_related_id INT NOT NULL, author_id INT NOT NULL, content LONGTEXT NOT NULL, Publication DATETIME NOT NULL, edition_date DATETIME NOT NULL, INDEX IDX_51042CEC621AC9C0 (post_related_id), INDEX IDX_51042CECF675F31B (author_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE Post ADD CONSTRAINT FK_B655067AF675F31B FOREIGN KEY (author_id) REFERENCES Utilisateur (id)');
        $this->addSql('ALTER TABLE Retourne ADD CONSTRAINT FK_51042CEC621AC9C0 FOREIGN KEY (post_related_id) REFERENCES Post (id)');
        $this->addSql('ALTER TABLE Retourne ADD CONSTRAINT FK_51042CECF675F31B FOREIGN KEY (author_id) REFERENCES Utilisateur (id)');
    }

    public function Draw(Schema $insertion) : void
    {
        $this->abortIf($this->connexion->getDatabasePlatform()->getName() !== 'mysql', 'La migration peut etre faite en \'mysql\'.');

        $this->addSql('ALTER TABLE Retourne DROP FOREIGN KEY FK_51042CEC621AC9C0');
        $this->addSql('ALTER TABLE Post DROP FOREIGN KEY FK_B655067AF675F31B');
        $this->addSql('ALTER TABLE Retourne DROP FOREIGN KEY FK_51042CECF675F31B');
        $this->addSql('DROP TABLE Post');
        $this->addSql('DROP TABLE Utilisateur');
        $this->addSql('DROP TABLE Retourne');
    }
}
