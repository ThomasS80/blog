<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200323185816 extends AbstractMigration
{
    public function Description() : string
    {
        return '';
    }

    public function MiseAJour(Schema $insertion) : void
    {
        $this->abortIf($this->connexion->getDatabasePlatform()->getName() !== 'mysql', 'La migration ne peut etre faite qu ici \'mysql\'.');

        $this->addSql('ALTER TABLE Utilisateur CHANGE Roles Roles JSON NOT NULL');
        $this->addSql('ALTER TABLE Retourne ADD return_related_id INT DEFAULT NULL, CHANGE post_related_id post_related_id INT DEFAULT NULL, CHANGE author_id author_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE Retourne ADD CONSTRAINT FK_51042CEC8AF66A33 FOREIGN KEY (return_related_id) REFERENCES Retourne (id)');
        $this->addSql('CREATE INDEX IDX_51042CEC8AF66A33 ON Retourne (return_related_id)');
    }

    public function Draw(Schema $insertion) : void
    {
        $this->abortIf($this->connexion->getDatabasePlatform()->getName() !== 'mysql', 'La migration ne peut etre faite qu ici \'mysql\'.');

        $this->addSql('ALTER TABLE Retourne DROP FOREIGN KEY FK_51042CEC8AF66A33');
        $this->addSql('DROP INDEX IDX_51042CEC8AF66A33 ON Retourne');
        $this->addSql('ALTER TABLE Retourne DROP return_related_id, CHANGE post_related_id post_related_id INT NOT NULL, CHANGE author_id author_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE Utilisateur CHANGE Roles Roles LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`');
    }
}
