<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200323182700 extends AbstractMigration
{
    public function Description() : string
    {
        return '';
    }

    public function MiseAJour(Schema $insertion) : void
    {
        $this->abortIf($this->connexion->getDatabasePlatform()->getName() !== 'mysql', 'La migration ne peut etre faite qu ici \'mysql\'.');

        $this->addSql('ALTER TABLE Utilisateur CHANGE Roles Roles JSON NOT NULL');
    }

    public function Draw(Schema $insertion) : void
    {
        $this->abortIf($this->connexion->getDatabasePlatform()->getName() !== 'mysql', 'La migration ne peut etre faite qu ici \'mysql\'.');

        $this->addSql('ALTER TABLE Utilisateur CHANGE Roles Roles LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`');
    }
}
