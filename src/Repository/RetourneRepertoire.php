<?php

namespace App\Repository;

use App\Entity\Retourne;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Retourne|null find($id, $lockMode = null, $lockVersion = null)
 * @method Retourne|null findOneBy(array $criteria, array $orderBy = null)
 * @method Retourne[]    findAll()
 * @method Retourne[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RetourneRepertoire extends ServiceEntityRepository
{
    public function constructeur(ManagerRegistry $registre)
    {
        parent::constructeur($registre, Retourne::class);
    }

    // /**
    //  * @return Retourne[] Returns an array of Retourne objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Retourne
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
