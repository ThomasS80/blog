<?php

namespace App\Controller;

use App\Entity\Retourne;
use App\Form\RetourneType;
use App\Service\RetourneServ;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/Retourne")
 */
class RetourneControler extends AbstractController
{
    /**
     * @Route("/", name="Retourne_index", methods={"GET"})
     */
    public function index(RetourneServ $serv): Response
    {
        return $this->render('Retourne/index.html.twig', [
            'Retournes' => $serv->index(),
        ]);
    }

    /**
     * @Route("/new", name="Retourne_new", methods={"GET","POST"})
     */
    public function nouvelle(Request $req): Response
    {
        $Retourne = new Retourne();
        $forme = $this->createForm(RetourneType::class, $Retourne);
        $forme->handleRequest($req);

        if ($forme->isSubmitted() && $forme->isValid()) {
            $entite = $this->getDoctrine()->getManager();
            $entite->persist($Retourne);
            $entite->flush();

            return $this->redirectToRoute('Retourne_index');
        }

        return $this->render('Retourne/nouvelle.html.twig', [
            'Retourne' => $Retourne,
            'Forme' => $forme->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="Retourne_PageModifier", methods={"GET"})
     */
    public function Modif(Retourne $Retourne): Response
    {
        return $this->render('Retourne/PageModifier.html.twig', [
            'Retourne' => $Retourne,
        ]);
    }

    /**
     * @Route("/{id}/edition", name="Retourne_edition", methods={"GET","POST"})
     */
    public function edition(Request $req, Retourne $Retourne): Response
    {
        $forme = $this->createForm(RetourneType::class, $Retourne);
        $forme->handleRequest($req);

        if ($forme->isSubmitted() && $forme->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('Retourne_index');
        }

        return $this->render('Retourne/edition.html.twig', [
            'Retourne' => $Retourne,
            'Forme' => $forme->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="Retourne_Supprimer", methods={"Supprimer"})
     */
    public function Supprimer(Request $req, Retourne $Retourne): Response
    {
        if ($this->isCsrfTokenValid('Supprimer'.$Retourne->Identifiant(), $req->request->get(''))) {
            $entite = $this->getDoctrine()->getManager();
            $entite->remove($Retourne);
            $entite->flush();
        }

        return $this->redirectToRoute('Retourne_index');
    }
}
