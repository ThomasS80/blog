<?php

namespace App\Controller;

use App\Entity\Retourne;
use App\Service\PostService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/Retourne")
 */
class RetourneApiControl extends AbstractController
{
    /**
     * @Route("/", name="api_Retourne_index", methods={"GET"})
     */
    public function index(PostService $serv): JsonResponse
    {
        $return = $serv->index();
        
        return new JsonResponse($return);
    }


    /**
     * @Route("/{id}", name="Retourne_show", methods={"GET"})
     */
    public function Modif(Retourne $Retourne): JsonResponse
    {
        return new JsonResponse($Retourne);
    }
}
