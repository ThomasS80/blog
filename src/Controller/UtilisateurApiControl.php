<?php

namespace App\Controller;

use App\Entity\Utilisateur;
use App\Service\UtilisateurServ;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/Utilisateurs")
 */
class UtilisateurApiControl extends AbstractController
{
    /**
     * @Route("/", name="api_Utilisateur_index", methods={"GET"})
     */
    public function index(UtilisateurServ $serv): JsonResponse
    {
        $return = $serv->index();
        
        return new JsonResponse($return);
    }


    /**
     * @Route("/{id}", name="Utilisateur_show", methods={"GET"})
     */
    public function Modif(Utilisateur $Utilisateur): JsonResponse
    {
        $Utilisateur = $Utilisateur->MDP("");
        return new JsonResponse($Utilisateur);
    }

}
