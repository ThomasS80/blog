<?php

namespace App\Controller;

use App\Entity\Post;
use App\Service\PostService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/Post")
 */
class PostApi extends AbstractController
{
    /**
     * @Route("/", name="api_ownpost_index", methods={"GET"})
     */
    public function index(PostService $serv) : JsonResponse
    {
        $posts = $serv->index();
        
        return new JsonResponse($posts);
    }

    /**
     * @Route("/{id}", name="api_ownpost_show", methods={"GET"})
     */
    public function show(Post $Post) : JsonResponse
    {
        return new JsonResponse($Post);
    }

}
