<?php

namespace App\Controller;

use App\Entity\Utilisateur;
use App\Form\UtilisateurType;
use App\Form\UtilisateurBis;
use App\Repository\UtilisateurRepertoire;
use App\Service\UtilisateurServ;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/Utilisateur")
 */
class UtilisateurControler extends AbstractController
{
    /**
     * @Route("/", name="Utilisateur_index", methods={"GET"})
     */
    public function index(UtilisateurServ $serv): Response
    {
        return $this->render('Utilisateur/index.html.twig', [
            'Utilisateurs' => $serv->index(),
        ]);
    }

    /**
     * @Route("/new", name="Utilisateur_new", methods={"GET","POST"})
     */
    public function nouvelle(UtilisateurServ $serv, Request $req, UserPasswordEncoderInterface $mdp): Response
    {
        $Utilisateur = new Utilisateur();
        $forme = $this->createForm(UtilisateurType::class, $Utilisateur);
        $forme->handleRequest($req);

       $donnee = $serv->nouvelle($this, $Utilisateur, $forme, $mdp);

        if ($donnee['redirect'] != null)
        {
            return $this->redirectToRoute($donnee['redirect']);
        }

        return $this->render('Utilisateur/nouvelle.html.twig', [
            'Utilisateur' =>$donnee["Utilisateur"],
            'Forme' =>$donnee["Forme"]->createView(),
            'Erreur' =>$donnee["Erreur"]
        ]);
    }

    /**
     * @Route("/{id}", name="Utilisateur_PageModifier", methods={"GET"})
     */
    public function Modif(Utilisateur $Utilisateur): Response
    {
        return $this->render('Utilisateur/PageModifier.html.twig', [
            'Utilisateur' => $Utilisateur,
        ]);
    }

    /**
     * @Route("/{id}/edition", name="Utilisateur_edition", methods={"GET","POST"})
     */
    public function edition(Request $req, Utilisateur $Utilisateur): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        if ($this->Utilisateur()->Identifiant() == $Utilisateur->Identifiant())
        {

        }
        $forme = $this->createForm(UtilisateurSimplifiedType::class, $Utilisateur);
        $forme->handleRequest($req);

        if ($forme->isSubmitted() && $forme->isValid() && filter_var($Utilisateur->mail(), FILTER_VALIDATE_EMAIL))
        {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('Utilisateur_index');
        }
        $erreur = null;
        if (!filter_var($Utilisateur->mail(), FILTER_VALIDATE_EMAIL))
        {
            $erreur = "Erreur mail";
        }
        return $this->render('Utilisateur/edition.html.twig', [
            'Utilisateur' => $Utilisateur,
            'Forme' => $forme->createView(),
            'erreur' => $erreur
        ]);
    }

    /**
     * @Route("/{id}", name="Utilisateur_Supprimer", methods={"Supprimer"})
     */
    public function Supprimer(Request $req, Utilisateur $Utilisateur): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        if ($this->isCsrfTokenValid('Supprimer'.$Utilisateur->Identifiant(), $req->request->get(''))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($Utilisateur);
            $entityManager->flush();
        }

        return $this->redirectToRoute('Utilisateur_index');
    }
}
