<?php

namespace App\Controller;

use App\Entity\Post;
use App\Form\PostType;
use App\Repository\PostRepertoire;
use App\Service\PostService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/")
 */
class PostControler extends AbstractController
{
    /**
     * @Route("/", name="home", methods={"GET"})
     */
    public function index(PostService $serv): Response
    {
        return $this->render('Post/index.html.twig', [
            'Posts' => $serv->index(),
        ]);
    }

    /**
     * @Route("/Post/new", name="Post_new", methods={"GET","POST"})
     */
    public function nouvelle(Request $req): Response
    {
        $Post = new Post();
        $form = $this->createForm(PostType::class, $Post);
        $form->handleRequest($req);

        if ($form->isSubmitted() && $form->isValid()) {
            $entite = $this->getDoctrine()->getManager();
            $entite->persist($Post);
            $entite->flush();

            return $this->redirectToRoute('home');
        }

        return $this->render('Post/new.html.twig', [
            'Post' => $Post,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/Post/{id}", name="Post_show", methods={"GET"})
     */
    public function Modif(Post $Post): Response
    {
        return $this->render('Post/show.html.twig', [
            'Post' => $Post,
        ]);
    }

    /**
     * @Route("Post/{id}/edit", name="Post_edit", methods={"GET","POST"})
     */
    public function edition(Request $req, Post $Post): Response
    {
        $form = $this->createForm(PostType::class, $Post);
        $form->handleRequest($req);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('home');
        }

        return $this->render('Post/edit.html.twig', [
            'Post' => $Post,
            'forme' => $form->createView(),
        ]);
    }

    /**
     * @Route("Post/{id}", name="Post_delete", methods={"DELETE"})
     */
    public function supprimer(Request $req, Post $Post): Response
    {
        if ($this->isCsrfTokenValid('Supprimer'.$Post->Identifiant(), $req->request->get('_token'))) {
            $entite = $this->getDoctrine()->getManager();
            $entite->remove($Post);
            $entite->flush();
        }

        return $this->redirectToRoute('home');
    }
}
