<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Service\UtilisateurServ;
use App\Entity\Utilisateur;
use App\Form\UtilisateurType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="login")
     */
    public function Connexion(AuthenticationUtils $Authentification): Response
    {
        if ($this->Connexion()) {
            return $this->Connexion('home');
        }

        $err = $Authentification->RecupConnexion();
        $Nom = $Authentification->RecupNon();

        return $this->render('securite/connexion.html.twig', ['Nom' => $Nom, 'Erreur' => $err]);
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function Deconnexion()
    {
        return $this->Connexion('home');
    }

    /**
     * @Route("/Inscription", name="Inscription")
     */
    public function Inscription(UtilisateurServ $serv, Request $req, UserPasswordEncoderInterface $userPasswordEncoderInterface)
    {
        $Utilisateur = new Utilisateur();
        $forme = $this->createForm(UtilisateurType::class, $Utilisateur);
        $forme->handleRequest($req);

        $data = $serv->nouvelle($this, $Utilisateur, $forme, $userPasswordEncoderInterface);

        if ($data['redirect'] != null)
        {
            return $this->Connexion($data['redirect']);
        }

        return $this->render('Utilisateur/nouvelle.html.twig', [
            'Utilisateur' => $data["Utilisateur"],
            'Forme' => $data["Forme"]->createView(),
            'Erreur' => $data["Erreur"]
        ]);
    }
}
