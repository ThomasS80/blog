<?php

namespace App\Service;

use App\Entity\Post;
use Doctrine\ORM\EntityManagerInterface;

class PostService
{
    /**
     * @var EntityManagerInterface
     */
    private $var;

    public function constructeur(EntityManagerInterface $var)
    {
        $this->em = $var;
    }

    public function index()
    {
        $rep = $this->em->getRepository(Post::class);

        $Post = $rep->findAll();

        return $Post;
    }
}
