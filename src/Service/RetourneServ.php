<?php

namespace App\Service;

use App\Entity\Retourne;
use Doctrine\ORM\EntityManagerInterface;

class RetourneServ
{
    /**
     * @var EntityManagerInterface
     */
    private $var;

    public function constructeur(EntityManagerInterface $var)
    {
        $this->em = $var;
    }

    public function index()
    {
        $rep = $this->em->getRepository(Retourne::class);

        $retourne = $rep->findAll();

        return $retourne;
    }
}
