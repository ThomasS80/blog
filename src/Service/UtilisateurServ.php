<?php

namespace App\Service;

use App\Entity\Utilisateur;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Form\UtilisateurType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\Test\FormInterface;

class UtilisateurServ
{
    /**
     * @var EntityManagerInterface
     */
    private $var;

    public function __construct(EntityManagerInterface $var)
    {
        $this->em = $var;
    }

    public function index()
    {
        $rep = $this->em->getRepository(Utilisateur::class);

        $utilisateur = $rep->findAll();

        return $utilisateur;
    }

    public function nouvelle(AbstractController $context, Utilisateur $utilisateur, Form $forme, UserPasswordEncoderInterface $MDP)
    {
        $redirection = null;
        if ($forme->isSubmitted() && $forme->isValid() && filter_var($utilisateur->getEmail(), FILTER_VALIDATE_EMAIL)) {
            $utilisateur->setRoles(['Utilisateur']);
            $utilisateur->setPassword($MDP->encodePassword($utilisateur, $utilisateur->getPassword()));
            $this->em->persist($utilisateur);
            $this->em->flush();
            $redirection = "home";
        }
        $error = null;
        if (!filter_var($utilisateur->getEmail(), FILTER_VALIDATE_EMAIL) && strlen($utilisateur->getEmail()) > 0)
        {
            $error = "Email Faux";
        }
        return ["Erreur" => $error, "Forme" => $forme, "Utilisateur" => $utilisateur, "Redirection" => $redirection];
    }
}
