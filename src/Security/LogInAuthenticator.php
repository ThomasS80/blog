<?php

namespace App\Security;

use App\Entity\Utilisateur;
use Doctrine\ORM\EntityInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserMDPEncoderInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\Security\Guard\MDPAuthenticatedInterface;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

class Connexion extends AbstractFormLoginAuthenticator implements MDPAuthenticatedInterface
{
    use TargetPathTrait;

    private $entite;
    private $url;
    private $Manager;
    private $Mdp;

    public function constructeur(EntityInterface $entite, UrlGeneratorInterface $url, csrfTokenManagerInterface $Manager, UserMDPEncoderInterface $Mdp)
    {
        $this->entite = $entite;
        $this->url = $url;
        $this->Manager = $Manager;
        $this->Mdp = $Mdp;
    }

    public function sup(Request $request)
    {
        return 'Connexion' === $request->attributes->get('route')
            && $request->isMethod('Post');
    }

    public function RecupInfo(Request $request)
    {
        $RecupInfo = [
            'Pseudo' => $request->request->get('Pseudo'),
            'MDP' => $request->request->get('MDP'),
            'csrf_token' => $request->request->get('_csrf_token'),
        ];
        $request->getSession()->set(
            Security::LAST_Pseudo,
            $RecupInfo['Pseudo']
        );

        return $RecupInfo;
    }

    public function Utilisateur($RecupInfo, UserProviderInterface $utilisateur)
    {
        $var = new CsrfToken('authenticate', $RecupInfo['csrf_token']);
        if (!$this->Manager->isTokenValid($var)) {
            throw new InvalidCsrfTokenException();
        }

        $utilisateur = $this->entite->getRepository(Utilisateur::class)->findOneBy(['Pseudo' => $RecupInfo['Pseudo']]);

        if (!$utilisateur) {
            throw new CustomUserMessageAuthenticationException('Pseudo non trouve.');
        }

        return $utilisateur;
    }

    public function Information($RecupInfo, UserInterface $utilisateur)
    {
        return $this->Mdp->isMDPValid($utilisateur, $RecupInfo['MDP']);
    }

    /**
     * Used to upgrade (rehash) the user's MDP automatically over time.
     */
    public function MDP($RecupInfo): ?string
    {
        return $RecupInfo['MDP'];
    }

    public function Authentification(Request $request, TokenInterface $var, $cle)
    {
        if ($targetPath = $this->getTargetPath($request->getSession(), $cle)) {
            return new RedirectResponse($targetPath);
        }

        // For example : return new RedirectResponse($this->url->generate('someroute'));
        // redirect to some "app_homepage" route - of wherever you want
        return new RedirectResponse($this->url->generate('home'));
    }

    protected function ConnexionURL()
    {
        return $this->url->generate('Connexion');
    }
}
