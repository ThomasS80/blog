<?php

namespace App\Form;

use App\Entity\Post;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PostType extends AbstractType
{
    public function Constructeur(FormBuilderInterface $Construc, array $opts)
    {
        $Construc
            ->add('Contenu')
            ->add('Titre')
        ;
    }

    public function Options(OptionsResolver $res)
    {
        $res->setDefaults([
            'Donnee' => Post::class,
        ]);
    }
}
