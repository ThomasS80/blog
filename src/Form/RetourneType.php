<?php

namespace App\Form;

use App\Entity\Retourne;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RetourneType extends AbstractType
{
    public function Constructeur(FormBuilderInterface $construc, array $opts)
    {
        $construc
            ->add('Contenu')
            ->add('mail')
        ;
    }

    public function Options(OptionsResolver $res)
    {
        $res->setDefaults([
            'donnee' => Retourne::class,
        ]);
    }
}
