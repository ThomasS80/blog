<?php

namespace App\Form;

use App\Entity\Utilisateur;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UtilisateurType extends AbstractType
{
    public function Constructeur(FormBuilderInterface $construc, array $opts)
    {
        $construc
            ->add('Prenom')
            ->add('MDP')
            ->add('Mail')
        ;
    }

    public function Options(OptionsResolver $res)
    {
        $res->setDefaults([
            'donnee' => Utilisateur::class,
        ]);
    }
}
