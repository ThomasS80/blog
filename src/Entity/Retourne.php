<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RetourneRepository")
 */
class Retourne
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $Identifiant;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Post", inversedBy="Retournes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Post;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Utilisateur", inversedBy="Retournes")
     * @ORM\JoinColumn(nullable=true)
     */
    private $Auteur;

    /**
     * @ORM\Column(type="text")
     */
    private $Contenu;

    /**
     * @ORM\Column(type="datetime")
     */
    private $Publication;

    /**
     * @ORM\Column(type="datetime")
     */
    private $edition;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $mail;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Retourne", inversedBy="Retournes")
     */
    private $Retourne;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Retourne", mappedBy="Retourne")
     */
    private $Retournes;

    public function constructeur()
    {
        $this->Retournes = new ArrayCollection();
    }

    public function Identifiant(): ?int
    {
        return $this->Identifiant;
    }

    public function Post(): ?Post
    {
        return $this->Post;
    }

    public function PostR(?Post $Post): self
    {
        $this->Post = $Post;

        return $this;
    }

    public function Auteur(): ?Utilisateur
    {
        return $this->Auteur;
    }

    public function AuteurR(?Utilisateur $Auteur): self
    {
        $this->Auteur = $Auteur;

        return $this;
    }

    public function Contenu(): ?string
    {
        return $this->Contenu;
    }

    public function ContenuR(string $Contenu): self
    {
        $this->Contenu = $Contenu;

        return $this;
    }

    public function Publication(): ?\DateTimeInterface
    {
        return $this->Publication;
    }

    public function PublicationR(\DateTimeInterface $Publication): self
    {
        $this->Publication = $Publication;

        return $this;
    }

    public function edition(): ?\DateTimeInterface
    {
        return $this->edition;
    }

    public function editionR(\DateTimeInterface $edition): self
    {
        $this->edition = $edition;

        return $this;
    }

    public function mail(): ?string
    {
        return $this->mail;
    }

    public function mailR(string $mail): self
    {
        $this->mail = $mail;

        return $this;
    }

    public function Retourne(): ?self
    {
        return $this->Retourne;
    }

    public function RetourneR(?self $Retourne): self
    {
        $this->Retourne = $Retourne;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getRetournes(): Collection
    {
        return $this->Retournes;
    }

    public function AddRetourne(self $Retourne): self
    {
        if (!$this->Retournes->contains($Retourne)) {
            $this->Retournes[] = $Retourne;
            $Retourne->Retournes($this);
        }

        return $this;
    }

    public function RemoveReturn(self $Retourne): self
    {
        if ($this->Retournes->contains($Retourne)) {
            $this->Retournes->removeElement($Retourne);
            if ($Retourne->getRetourne() === $this) {
                $Retourne->Retournes(null);
            }
        }

        return $this;
    }
}
