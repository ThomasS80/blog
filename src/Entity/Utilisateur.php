<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Encoder\UserMDPEncoder;
use Symfony\Component\Security\Core\Encoder\UserMDPEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Validator\Constraints\UserMDP;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OwnUserRepository")
 */
class Utilisateur implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $Identifiant;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $Prenom;

    /**
     * @ORM\Column(type="json")
     */
    private $Roles = [];

    /**
     * @var string The hashed MDP
     * @ORM\Column(type="string")
     */
    private $MDP;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Post", mappedBy="Author")
     */
    private $Post;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Retourne", mappedBy="Author")
     */
    private $Retourne;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $mail;

    public function constructeur()
    {
        $this->Post = new ArrayCollection();
        $this->Retourne = new ArrayCollection();
    }

    public function Identifiant(): ?int
    {
        return $this->Identifiant;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function Prenom(): string
    {
        return (string) $this->Prenom;
    }

    public function PrenomR(string $Prenom): self
    {
        $this->Prenom = $Prenom;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function Roles(): array
    {
        $Roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $Roles[] = 'Utilisateur';

        return array_unique($Roles);
    }

    public function RolesR(array $Roles): self
    {
        $this->roles = $Roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function MDP(): string
    {
        return (string) $this->MDP;
    }

    public function MDPR(string $MDP): self
    {
        $this->MDP = $MDP;

        return $this;
    }

    /**
     * @return Collection|Post[]
     */
    public function Post(): Collection
    {
        return $this->Post;
    }

    public function Post2(Post $Post): self
    {
        if (!$this->Post->contains($Post)) {
            $this->Post[] = $Post;
            $Post->Auteur($this);
        }

        return $this;
    }

    public function PostR(Post $Post): self
    {
        if ($this->Post->contains($Post)) {
            $this->Post->removeElement($Post);
            if ($Post->Auteur() === $this) {
                $Post->Auteur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Retourne[]
     */
    public function getRetourne(): Collection
    {
        return $this->Retourne;
    }

    public function Retourne(Retourne $Retourne): self
    {
        if (!$this->Retourne->contains($Retourne)) {
            $this->Retourne[] = $Retourne;
            $Retourne->Auteur($this);
        }

        return $this;
    }

    public function removeRetourne(Retourne $Retourne): self
    {
        if ($this->Retourne->contains($Retourne)) {
            $this->Retourne->removeElement($Retourne);
            if ($Retourne->getAuthor() === $this) {
                $Retourne->Auteur(null);
            }
        }

        return $this;
    }

    public function mail(): ?string
    {
        return $this->Email;
    }

    public function mailR(string $mail): self
    {
        $this->Email = $mail;
        
        return $this;
    }

    /**
     * @return Boolean
     */
    public function Posthere(Post $Post)
    {
        foreach ($this->Post() as $post)
        {
            if ($post->Identifiant() == $Post->Identifiant())
            {
                return true;
            }
        }
        return false;
    }
    /**
     * @return Boolean
     */
    public function RetourneHere(Retourne $Retourne)
    {
        foreach ($this->Retourne() as $return)
        {
            if ($return->Identifiant() == $Retourne->Identifiant())
            {
                return true;
            }
        }
        return false;
    }
}
