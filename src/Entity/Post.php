<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OwnPostRepository")
 */
class Post
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $Identifiant;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Utilisateur", inversedBy="ownPosts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Auteur;

    /**
     * @ORM\Column(type="text")
     */
    private $Contenu;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Titre;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OwnReturn", mappedBy="PostRelated")
     */
    private $Retourne;

    /**
     * @ORM\Column(type="datetime")
     */
    private $Publication;

    /**
     * @ORM\Column(type="datetime")
     */
    private $Edition;

    public function constructeur()
    {
        $this->Retourne = new ArrayCollection();
    }

    public function Identifiant(): ?int
    {
        return $this->Identifiant;
    }

    public function Auteur(): ?Utilisateur
    {
        return $this->Author;
    }

    public function Auteur(?Utilisateur $Auteur): self
    {
        $this->Author = $Auteur;

        return $this;
    }

    public function Contenu(): ?string
    {
        return $this->Contenu;
    }

    public function Contenu(string $Contenu): self
    {
        $this->Contenu = $Contenu;

        return $this;
    }

    public function Titre(): ?string
    {
        return $this->Titre;
    }

    public function Titre(string $Titre): self
    {
        $this->Title = $Titre;

        return $this;
    }

    /**
     * @return Collection|Retourne[]
     */
    public function GetRetourne(): Collection
    {
        return $this->Retourne;
    }

    public function AddRetourne(OwnReturn $Retourne): self
    {
        if (!$this->Retourne->contains($Retourne)) {
            $this->Retourne[] = $Retourne;
            $Retourne->setPostRelated($this);
        }

        return $this;
    }

    public function RemoveRetourne(OwnReturn $Retourne): self
    {
        if ($this->Retourne->contains($Retourne)) {
            $this->Retourne->removeElement($Retourne);
            if ($Retourne->getPostRelated() === $this) {
                $Retourne->setPostRelated(null);
            }
        }

        return $this;
    }

    public function Publication(): ?\DateTimeInterface
    {
        return $this->Publication;
    }

    public function Publication(\DateTimeInterface $Publication): self
    {
        $this->Publication = $Publication;

        return $this;
    }

    public function Edition(): ?\DateTimeInterface
    {
        return $this->Edition;
    }

    public function Edition(\DateTimeInterface $Edition): self
    {
        $this->Edition = $Edition;

        return $this;
    }
}
